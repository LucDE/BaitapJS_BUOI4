// DOM tới thẻ ID
function dom_id(id) {
  return document.getElementById(id);
}
// Lấy giá trị của ID
function dom_id_value(id) {
  var tagID = document.getElementById(id);
  return tagID.value;
}
// Trả giá trị về 0
function dom_id_value_return_0(id) {
  var tagID = document.getElementById(id);
  return (tagID.value = "");
}
// Thêm item vào cuối mảng
function add_item_to_end(array_A, item) {
  var array_A;
  var item;
  return (array_A = array_A.push(Number(item)));
}
// Thêm nội dung vào bên trong thẻ ID
// Note: ID phải cho vào dấu nháy
function put_text_into_id(id, text_content) {
  return (dom_id(id).innerHTML = text_content);
}
// Sắp xếp số nhập vào theo thứ tự tăng dần
function arrange_min_to_max(array, new_arange_array) {
  var a = 0;
  // không khai báo lại giá trị phía trên hàm, chỉ lấy giá trị vào từ return
  //   var array = []; sai
  //   var new_arange_array = []; sai
  // không nên đặt các biến giống với biến của bài chính
  var number = 0;
  var min = 0;
  number = array.length;
  for (a = 0; a < number; a++) {
    min = array[0];
    for (i = 0; i < array.length; i++) {
      if (array[i] < min) {
        min = array[i];
      }
    }
    new_arange_array.push(Number(min));
    array.splice(array.indexOf(Number(min)), 1);
  }
  return new_arange_array;
}
