//Bài 4.1: Sắp xếp số nguyên tăng dần
var count_number = 0;
var _number = 0;
var array_A = [];
var array_B = [];
var text4_1;
var element_p2;

// rảnh có thể viết thành 1 hàm lấy giá trị người dùng nhập cho vào mảng
function take_number_value() {
  _number = dom_id_value("number_input");
  count_number++;
  if (count_number < 4) {
    //có biến count_number của bài chính nên không dùng put_text được
    text4_1 = "Số thứ " + count_number + " của bạn là " + _number;
    add_item_to_end(array_A, _number);
  } else {
    put_text_into_id("alert_1", "Vui lòng nhập 3 số");
    return (
      (count_number = 0), dom_id_value_return_0("number_input"), (array_A = [])
    );
  }
  put_text_into_id("alert_1", text4_1);
  return array_A, dom_id_value_return_0("number_input");
}
function arrange_number() {
  if (count_number == 3) {
    arrange_min_to_max(array_A, array_B);
    put_text_into_id(
      "alert_1",
      "3 số theo thứ tự tăng dần là: " +
        array_B[0] +
        " " +
        array_B[1] +
        " " +
        array_B[2]
    );
    return (count_number = 0), (array_A = []), (array_B = []);
  } else {
    put_text_into_id("alert_1", "Vui lòng nhập 3 số");
  }
}
// Bài 4.2: Chương trình chào hỏi
var _character;
function take_string_value() {
  _character = dom_id_value("string_input");
  switch (_character) {
    case "B":
      put_text_into_id("alert_2", "Xin chào Bố!");
      break;
    case "M":
      put_text_into_id("alert_2", "Xin chào Mẹ!");
      break;
    case "A":
      put_text_into_id("alert_2", "Xin chào Anh!");
      break;
    case "E":
      put_text_into_id("alert_2", "Xin chào Em!");
      break;
    default:
      put_text_into_id("alert_2", "Vui lòng nhập đúng đặc điểm");
  }
}

// Bài 4.3: Số chẵn và số lẻ
var number_Input;
var even_number = 0;
var odd_number = 0;
var count__number = 0;

function get_number() {
  count__number++;
  number_Input = dom_id_value("number__input");
  if (count__number < 4) {
    if (number_Input % 2 == 0) {
      even_number++;
    } else {
      odd_number++;
    }
    put_text_into_id(
      "alert_3",
      "Số thứ " + count__number + " của bạn là: " + number_Input
    );
  } else {
    put_text_into_id("alert_3", "Vui lòng nhập 3 số!");
    return (odd_number = 0), (even_number = 0), (count__number = 0);
  }
}
function even_odd() {
  put_text_into_id(
    "alert_3",
    "Có " + even_number + " số chẵn và " + odd_number + " số lẻ."
  );
  return (odd_number = 0), (even_number = 0), (count__number = 0);
}

// Bài 4.4: Xác định tam giác nhờ 3 cạnh
var text4_4;
var count_Number = 0;
var sides_array = [];
var side1 = 0;
var side2 = 0;
var side3 = 0;

function get_Num() {
  side_length = dom_id_value("side_Input");
  count_Number++;
  if (count_Number < 4) {
    text4_4 = "Độ dài cạnh thứ " + count_Number + " của bạn là " + side_length;
    add_item_to_end(sides_array, side_length);
  } else {
    put_text_into_id("alert_4", "Vui lòng nhập 3 cạnh");
    return (
      (count_Number = 0),
      dom_id_value_return_0("side_Input"),
      (sides_array = [])
    );
  }
  put_text_into_id("alert_4", text4_4);
  return (
    sides_array,
    (side1 = sides_array[0]),
    (side2 = Number(sides_array[1])),
    (side3 = Number(sides_array[2])),
    dom_id_value_return_0("side_Input")
  );
}
function triangle() {
  if (count_Number == 3) {
    if (
      Math.pow(side1, 2) === Math.pow(side2, 2) + Math.pow(side3, 2) ||
      Math.pow(side2, 2) === Math.pow(side1, 2) + Math.pow(side3, 2) ||
      Math.pow(side3, 2) === Math.pow(side1, 2) + Math.pow(side2, 2)
    ) {
      put_text_into_id("alert_4", "Tam giác vuông.");
    } else if (
      side1 - side2 === 0 &&
      side1 - side3 === 0 &&
      side2 - side3 === 0
    ) {
      put_text_into_id("alert_4", "Tam giác đều.");
    } else if (side1 === side2 || side1 === side3 || side2 === side3) {
      put_text_into_id("alert_4", "Tam giác cân.");
    } else {
      put_text_into_id("alert_4", "Tam giác bình thường.");
    }
  } else {
    put_text_into_id("alert_4", "Vui lòng nhập 3 cạnh!");
    return (sides_array = []), (count_Number = 0);
  }
  return (sides_array = []), (count_Number = 0);
}
